#pragma once

#include "Pool.hpp"
#include <vector>
#include <algorithm>

namespace Tecs
{
    /* Pooler class for most of the dynamically-created objects. Instead of using ObjectPool<T>s
       directly, they are wrapped in this templated Pooler<T> class that creates "static" pools
       for each type T. Each T gets a singleton Pooler object. */
    template<class T>
    class Pooler
    {
    public:
        
        ~Pooler()
        {
            for (unsigned int i = 0; i < m_ptrs.size(); ++i)
            {
                free(m_ptrs[i]);
            }
        }

        T* create()
        {
            T* t = m_objs.New();
            m_ptrs.push_back(t);
            return t;
        }

        T* getNext()
        {
            T* t = m_objs.GetNextWithoutInitializing();
            m_ptrs.push_back(t);
            return t;
        }

        void free(T* t)
        {
            m_ptrs.erase(std::remove(m_ptrs.begin(), m_ptrs.end(), t), m_ptrs.end());
            m_objs.Delete(t);
        }

        static Pooler<T>& instance()
        {
            return m_instance;
        }
    private:
        Pooler()
        {
        }

        static Pooler<T> m_instance;

        ObjectPool<T> m_objs;
        std::vector<T*> m_ptrs;
    };

    template<class T>
    Pooler<T> Pooler<T>::m_instance;
}