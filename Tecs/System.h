#pragma once

#include "EntityObserver.h"
#include <vector>
#include <memory>
#include "Aspect.h"

namespace Tecs
{
    class World;

    /* Base class for all systems.
    
       A system is a module that performs certain tasks on certain
       components. For example, a PhysicsSystem would be responsible
       for updating the physics components of entities. */
    class System : public EntityObserver
    {
    public:
        // System();
        System(std::unique_ptr<Aspect> aspect);
        System(const System&) = delete;
        ~System();
        
        void process(float delta);

        virtual void entityAdded(unsigned int id) final;
        virtual void entityChanged(unsigned int id) final;
        virtual void entityDeleted(unsigned int id) final;
        virtual void entityEnabled(unsigned int id) final;
        virtual void entityDisabled(unsigned int id) final;

        void setWorld(World* world);
        bool isPassive();
        void setPassive(bool passive);
        // const std::vector<Entity*>& getActives();
        const std::vector<unsigned int>& getActives();
        
        virtual void initialize() {}


        void setSystemIndexInternal(unsigned int index);
    protected:
        World* m_world;
        virtual void begin();
        virtual void end();

        // virtual void processEntity(const Entity& entity, float delta) = 0;
        virtual void processEntities(const std::vector<unsigned int>& entities, float delta) = 0;

        virtual bool shouldProcess() { return true; }

        virtual void inserted(unsigned int entity) {}
        virtual void removed(unsigned int entity) {}

        void check(unsigned int entity);

    private:
        unsigned int m_systemIndex;

        std::vector<unsigned int> m_actives;
        const IndexedBitset& m_allSet;
        const IndexedBitset& m_exclusionSet;
        const IndexedBitset& m_oneSet;

        std::unique_ptr<Aspect> m_aspect;

        bool m_passive;
        bool m_dummy;

        void removeFromSystem(unsigned int entity);
        void insertToSystem(unsigned int entity);
    };
}