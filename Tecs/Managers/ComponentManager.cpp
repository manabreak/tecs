#include "ComponentManager.h"
#include "../Entity.h"
#include <algorithm>
#include "../World.h"

namespace Tecs
{
    ComponentManager::ComponentManager()
    {
    }

    ComponentManager::~ComponentManager()
    {
        for (unsigned int i = 0; i < m_componentsByType.size(); ++i)
        {
            for (unsigned int j = 0; j < m_componentsByType[i].size(); ++j)
            {
                if (m_componentsByType[i][j] != nullptr)
                {
                    m_componentsByType[i][j]->_callDtorInternal();
                }
            }
        }
    }

    void ComponentManager::removeComponentsOfEntity(unsigned int entity)
    {
        IndexedBitset& componentBits = m_world->getComponentMapper().getComponentBitset(entity);
        for (unsigned int i = 0; i < componentBits.size(); ++i)
        {
            if (componentBits.get(i))
            {
                m_componentsByType[i][entity]->_callDtorInternal();
                m_componentsByType[i][entity] = nullptr;
            }
        }
        
        componentBits.clear();
    }

    void ComponentManager::ensureCapacity(unsigned int typeIndex, unsigned int entityId)
    {
        if (typeIndex >= m_componentsByType.size())
        {
            m_componentsByType.resize(typeIndex * 3 / 2);
        }
        if (entityId >= m_componentsByType[typeIndex].size())
        {
            m_componentsByType[typeIndex].resize(entityId * 3 / 2);
        }

        if (entityId >= m_componentsByEntity.size())
        {
            m_componentsByEntity.resize(entityId * 3 / 2);
        }
        if (typeIndex >= m_componentsByEntity[entityId].size())
        {
            m_componentsByEntity[entityId].resize(typeIndex * 3 / 2);
        }
    }

    void ComponentManager::flagEntityComponentBit(unsigned int entity, unsigned int index, bool status)
    {
        m_world->getComponentMapper().getComponentBitset(entity).set(index, status);
    }

    void ComponentManager::removeComponent(unsigned int entity, const Type<Component>& type)
    {
        unsigned int index = type.getIndex();
        IndexedBitset& componentBits = m_world->getComponentMapper().getComponentBitset(entity);
        if (componentBits.get(index))
        {
            std::swap(m_componentsByType[index][entity], m_componentsByType[index].back());
            m_componentsByType[index].pop_back();
            componentBits.set(index, false);
        }
    }

    const std::vector<Component*>& ComponentManager::getComponentsFor(unsigned int entity)
    {
        while (entity >= m_componentsByEntity.size())
        {
            m_componentsByEntity.push_back(std::vector<Component*>());
        }
        return m_componentsByEntity[entity];
    }

    void ComponentManager::entityDeleted(unsigned int entity)
    {
        m_deleted.push_back(entity);
        if (!m_world->isProcessing())
        {
            clean();
        }
    }

    void ComponentManager::clean()
    {
        if (m_deleted.size() > 0)
        {
            for (unsigned int i = 0; i < m_deleted.size(); ++i)
            {
                removeComponentsOfEntity(m_deleted[i]);
            }
            m_deleted.clear();
        }
    }

    Component* Tecs::ComponentManager::getComponentOfEntity(unsigned int idxOfComponent, unsigned int entity) const
    {
        return m_componentsByType[idxOfComponent][entity];
    }
}
