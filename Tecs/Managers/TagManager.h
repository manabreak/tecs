#pragma once

#include "Manager.h"
#include <unordered_map>
#include <string>
#include <unordered_set>
#include <vector>

namespace Tecs
{
    class TagManager : public Manager
    {
    public:
        TagManager();
        void setTag(const std::string& tag, unsigned int entity);
        const std::string& getTag(unsigned int entity);
        void unsetTag(const std::string& tag);
        bool isTagSet(const std::string& tag);
        const std::vector<std::string>& getAllTags();
        virtual void entityDeleted(unsigned int entity);
        virtual void initialize() {}
    protected:

    private:
        std::vector<std::string> m_tagsByEntity;
    };
}
