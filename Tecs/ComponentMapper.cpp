#include "ComponentMapper.h"
#include "World.h"
#include "Managers/ComponentManager.h"

namespace Tecs
{
    ComponentMapper::ComponentMapper(World& world)
        : m_world(world)
    {

    }

    ComponentMapper::~ComponentMapper()
    {

    }

    void ComponentMapper::entityCreated(unsigned int entity)
    {
        if (m_systemBits.size() <= entity)
        {
            m_systemBits.resize(entity * 3 / 2 + 1);
        }
        if (m_componentBits.size() <= entity)
        {
            m_componentBits.resize(entity * 3 / 2 + 1);
        }

        m_systemBits[entity] = IndexedBitset();
        m_componentBits[entity] = IndexedBitset();
    }

    void ComponentMapper::entityRemoved(unsigned int entity)
    {
        if (entity < m_systemBits.size())
        {
            m_systemBits[entity].clear();
        }
        if (entity < m_componentBits.size())
        {
            m_componentBits[entity].clear();
        }
    }

    void ComponentMapper::entityDisabled(unsigned int entity)
    {

    }

    void ComponentMapper::entityEnabled(unsigned int entity)
    {

    }

    void ComponentMapper::entityChanged(unsigned int entity)
    {

    }

    void ComponentMapper::attachComponent(unsigned int entity, unsigned int componentType)
    {
        m_componentBits[entity].set(componentType, true);
    }

    void ComponentMapper::detachComponent(unsigned int entity, unsigned int componentType)
    {
        m_componentBits[entity].set(componentType, false);
    }

    bool ComponentMapper::isComponentAttached(unsigned int entity, unsigned int component)
    {
        return m_componentBits.size() > entity && m_componentBits[entity].get(component);
    }

    IndexedBitset& ComponentMapper::getComponentBitset(unsigned int entity)
    {
        if (m_componentBits.size() <= entity)
        {
            m_componentBits.resize(entity * 3 / 2 + 1);
        }
        return m_componentBits[entity];
    }

    void ComponentMapper::setSystemBit(unsigned int entity, unsigned int systemIndex, bool value)
    {
        m_systemBits[entity].set(systemIndex, value);
    }

    bool ComponentMapper::getSystemBit(unsigned int entity, unsigned int systemIndex)
    {
        if (m_systemBits.size() <= entity)
        {
            m_systemBits.resize(entity * 3 / 2 + 1);
        }
        return m_systemBits[entity].get(systemIndex);
    }

    IndexedBitset& ComponentMapper::getSystemBitset(unsigned int entity)
    {
        if (m_systemBits.size() <= entity)
        {
            m_systemBits.resize(entity * 3 / 2 + 1);
        }
        return m_systemBits[entity];
    }
}