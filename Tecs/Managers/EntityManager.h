#pragma once

#include "Manager.h"
#include <vector>
#include <unordered_set>
#include <memory>

namespace Tecs
{
    class EntityManager : public Manager
    {
    private:
        class IdentifierPool
        {
        public:
            IdentifierPool()
                : m_ids(),
                m_nextAvailableId(1)
            {
            }

            int checkOut()
            {
                if (m_ids.size() > 0)
                {
                    int id = m_ids.back();
                    m_ids.pop_back();
                    return id;
                }
                return m_nextAvailableId++;
            }

            void checkIn(int id)
            {
                m_ids.push_back(id);
            }
        private:
            std::vector<int> m_ids;
            int m_nextAvailableId;
        };

    public:
        EntityManager();
        ~EntityManager();

        virtual void entityAdded(unsigned int id);
        virtual void entityEnabled(unsigned int id);
        virtual void entityDisabled(unsigned int id);
        virtual void entityDeleted(unsigned int id);

        bool isEnabled(unsigned int id);

        unsigned int getEntityCount();
        unsigned long getTotalCreated();
        unsigned long getTotalAdded();
        unsigned long getTotalDeleted();

        unsigned int createEntityInstance();

        void clean();
    private:
        // std::vector<Entity*> m_entities;
        std::vector<unsigned int> m_entities;
        std::vector<unsigned int> m_deleteQueue;
        unsigned int m_active;
        unsigned long m_added;
        unsigned long m_created;
        unsigned long m_deleted;
        IdentifierPool m_identifierPool;
        std::vector<unsigned int> m_disabled;
    };
}