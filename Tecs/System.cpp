#include "System.h"
#include "Entity.h"
#include "World.h"

namespace Tecs
{


    System::System(std::unique_ptr<Aspect> aspect)
        : m_allSet(aspect->getAllSet()),
        m_exclusionSet(aspect->getExclusionSet()),
        m_oneSet(aspect->getOneSet()),
        m_passive(false)
    {
        m_aspect = std::move(aspect);
        m_dummy = m_allSet.isEmpty() && m_oneSet.isEmpty();
    }

    System::~System()
    {
        m_actives.clear();
    }

    void System::begin()
    {
    }

    void System::process(float delta)
    {
        if (shouldProcess())
        {
            begin();
            processEntities(m_actives, delta);
            end();
        }
    }

    void System::end()
    {
    }

    void System::check(unsigned int entity)
    {
        if (m_dummy) return;

        // bool contains = entity.getSystemBits().get(m_systemIndex);
        bool contains = m_world->getComponentMapper().getSystemBit(entity, m_systemIndex);
        bool interested = true;
        IndexedBitset& components = m_world->getComponentMapper().getComponentBitset(entity);

        if (!m_allSet.isEmpty())
        {
            for (unsigned int i = 0; i < m_allSet.size(); ++i)
            {
                if (!m_allSet.get(i)) continue;

                // if (!entity.getComponentBits().get(i))
                if (!m_world->getComponentMapper().isComponentAttached(entity, i))
                {
                    interested = false;
                    break;
                }
            }
        }

        if (!m_exclusionSet.isEmpty() && interested)
        {
            for (unsigned int i = 0; i < m_exclusionSet.size(); ++i)
            {
                if (i < components.size())
                {
                    if (components.get(i))
                    {
                        interested = false;
                        break;
                    }
                }
                else break;
            }
        }

        if (!m_oneSet.isEmpty())
        {
            for (unsigned int i = 0; i < m_oneSet.size(); ++i)
            {
                if (i < components.size())
                {
                    if (components.get(i))
                    {
                        interested = true;
                        break;
                    }
                }
                else
                {
                    interested = false;
                }
            }
        }

        if (interested && !contains)
        {
            insertToSystem(entity);
        }
        else if (!interested && contains)
        {
            removeFromSystem(entity);
        }
    }

    void System::removeFromSystem(unsigned int entity)
    {
        m_actives.erase(std::remove(m_actives.begin(), m_actives.end(), entity), m_actives.end());
        m_world->getComponentMapper().setSystemBit(entity, m_systemIndex, false);
        removed(entity);
    }

    void System::insertToSystem(unsigned int entity)
    {
        m_actives.push_back(entity);
        m_world->getComponentMapper().setSystemBit(entity, m_systemIndex, true);
        inserted(entity);
    }

    void System::entityAdded(unsigned int entity)
    {
        check(entity);
    }

    void System::entityChanged(unsigned int entity)
    {
        check(entity);
    }

    void System::entityDeleted(unsigned int entity)
    {
        // if (entity.getSystemBits().get(m_systemIndex))
        if (m_world->getComponentMapper().getSystemBit(entity, m_systemIndex))
        {
            removeFromSystem(entity);
        }
    }

    void System::entityDisabled(unsigned int entity)
    {
        // if (entity.getSystemBits().get(m_systemIndex))
        if (m_world->getComponentMapper().getSystemBit(entity, m_systemIndex))
        {
            removeFromSystem(entity);
        }
    }

    void System::entityEnabled(unsigned int entity)
    {
        check(entity);
    }

    void System::setWorld(World* world)
    {
        m_world = world;
    }

    void System::setSystemIndexInternal(unsigned int index)
    {
        m_systemIndex = index;
    }

    bool System::isPassive()
    {
        return m_passive;
    }

    void System::setPassive(bool passive)
    {
        m_passive = passive;
    }

    const std::vector<unsigned int>& System::getActives()
    {
        return m_actives;
    }
}