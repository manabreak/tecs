#include "World.h"

namespace Tecs
{
    World::World()
        : m_processing(false),
        m_componentMapper(*this)
    {
        addManager<ComponentManager>();
        addManager<EntityManager>();
        addManager<GroupManager>();
        addManager<TagManager>();
    }

    World::~World()
    {
        m_managers.clear();
        m_systems.clear();
    }

    EntityManager& World::getEntityManager()
    {
        return getManager<EntityManager>();
    }

    ComponentManager& World::getComponentManager()
    {
        return getManager<ComponentManager>();
    }

    TagManager& World::getTagManager()
    {
        return getManager<TagManager>();
    }

    GroupManager& World::getGroupManager()
    {
        return getManager<GroupManager>();
    }

    const std::vector<System*>& World::getSystems()
    {
        return m_systems;
    }

    /*
    void World::addEntity(unsigned int id)
    {
        m_added.push_back(id);
    }
    */

    void World::changedEntity(unsigned int id)
    {
        m_changed.push_back(id);
    }

    void World::deleteEntity(unsigned int id)
    {
        for (unsigned int i = 0; i < m_deleted.size(); ++i)
        {
            if (m_deleted[i] == id) return;
        }
        if (m_processing)
        {   
            m_deleted.push_back(id);
        }
        else
        {
            for (unsigned int j = 0; j < m_systems.size(); ++j)
            {
                if (m_systems[j] == nullptr) continue;
                m_systems[j]->entityDeleted(id);
            }
            for (unsigned int j = 0; j < m_managers.size(); ++j)
            {
                if (m_managers[j] == nullptr) continue;
                m_managers[j]->entityDeleted(id);
            }
        }
    }

    void World::enableEntity(unsigned int id)
    {
        m_enable.push_back(id);
    }

    void World::disableEntity(unsigned int id)
    {
        m_disable.push_back(id);
    }

    Entity World::createEntity()
    {
        Entity e(*this, getEntityManager().createEntityInstance());
        m_added.push_back(e.getId());
        return e;
    }

    float World::getDelta()
    {
        return m_delta;
    }

    void World::setDelta(float delta)
    {
        m_delta = delta;
    }

    void World::process()
    {
        process(m_delta);
    }

    void World::process(float delta)
    {
        processDeleted();
        processAdded();
        processChanged();
        processDisabled();
        processEnabled();

        getComponentManager().clean();
        getEntityManager().clean();

        m_processing = true;
        unsigned int systemCount = m_systems.size();
        for (unsigned int i = 0; i < systemCount; ++i)
        {
            if (m_systems[i] == nullptr) continue;
            if (!m_systems[i]->isPassive()) m_systems[i]->process(delta);
        }
        m_processing = false;
    }

    void World::processAdded()
    {
        if (!m_added.empty())
        {
            unsigned int addedCount = m_added.size();
            for (unsigned int i = 0; i < addedCount; ++i)
            {
                unsigned int addedEntity = m_added[i];

                unsigned int managerCount = m_managers.size();
                for (unsigned int j = 0; j < managerCount; ++j)
                {
                    if (m_managers[j] == nullptr) continue;
                    m_managers[j]->entityAdded(addedEntity);
                }
                unsigned int systemCount = m_systems.size();
                for (unsigned int j = 0; j < systemCount; ++j)
                {
                    if (m_systems[j] == nullptr) continue;
                    m_systems[j]->entityAdded(addedEntity);
                }
            }
            m_added.clear();
        }
    }

    void World::processChanged()
    {
        if (!m_changed.empty())
        {
            for (unsigned int i = 0; i < m_changed.size(); ++i)
            {
                for (unsigned int j = 0; j < m_managers.size(); ++j)
                {
                    if (m_managers[j] == nullptr) continue;
                    m_managers[j]->entityChanged(m_changed[i]);
                }
                for (unsigned int j = 0; j < m_systems.size(); ++j)
                {
                    if (m_systems[j] == nullptr) continue;
                    m_systems[j]->entityChanged(m_changed[i]);
                }
            }
            m_changed.clear();
        }
    }

    void World::processDeleted()
    {
        if (!m_deleted.empty())
        {
            for (unsigned int i = 0; i < m_deleted.size(); ++i)
            {
                for (unsigned int j = 0; j < m_systems.size(); ++j)
                {
                    if (m_systems[j] == nullptr) continue;
                    m_systems[j]->entityDeleted(m_deleted[i]);
                }
                for (unsigned int j = 0; j < m_managers.size(); ++j)
                {
                    if (m_managers[j] == nullptr) continue;
                    m_managers[j]->entityDeleted(m_deleted[i]);
                }
            }
            m_deleted.clear();
        }
    }

    void World::processDisabled()
    {
        if (!m_disable.empty())
        {
            for (unsigned int i = 0; i < m_disable.size(); ++i)
            {
                for (unsigned int j = 0; j < m_managers.size(); ++j)
                {
                    if (m_managers[j] == nullptr) continue;
                    m_managers[j]->entityDisabled(m_disable[i]);
                }
                for (unsigned int j = 0; j < m_systems.size(); ++j)
                {
                    if (m_systems[j] == nullptr) continue;
                    m_systems[j]->entityDisabled(m_disable[i]);
                }
            }
            m_disable.clear();
        }
    }

    void World::processEnabled()
    {
        if (!m_enable.empty())
        {
            for (unsigned int i = 0; i < m_enable.size(); ++i)
            {
                for (unsigned int j = 0; j < m_managers.size(); ++j)
                {
                    if (m_managers[j] == nullptr) continue;
                    m_managers[j]->entityEnabled(m_enable[i]);
                }
                for (unsigned int j = 0; j < m_systems.size(); ++j)
                {
                    if (m_systems[j] == nullptr) continue;
                    m_systems[j]->entityEnabled(m_enable[i]);
                }
            }
            m_enable.clear();
        }
    }

    int World::getEntityCount()
    {
        return getEntityManager().getEntityCount();
    }

    void World::processSystem(unsigned int index)
    {
        processSystem(index, m_delta);
    }

    void World::processSystem(unsigned int index, float delta)
    {
        if (index < m_systems.size())
        {
            m_systems[index]->process(delta);
        }
    }

    bool World::isProcessing()
    {
        return m_processing;
    }

    ComponentMapper& World::getComponentMapper()
    {
        return m_componentMapper;
    }
}