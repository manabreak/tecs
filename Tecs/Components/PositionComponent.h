#pragma once

#include "../Component.h"

namespace Tecs
{
    // A sample component to show the creation of new components.
    class PositionComponent : public Component
    {
    public:
        PositionComponent()
            : Component(),
            x(0.f),
            y(0.f),
            z(0.f)
        {

        }

        float x, y, z;
    };
}