#pragma once

#include "../System.h"
#include "../Components/PositionComponent.h"
#include "../Entity.h"

namespace Tecs
{
    // An example system to show how to create new systems.
    class PositionSystem : public System
    {
    public:
        PositionSystem()
            : System(Aspect::createAspectForOne<PositionComponent>())
        {
        }

        virtual void processEntities(const std::vector<unsigned int>& entities, float delta)
        {
            // Get the position component
            for (unsigned int i = 0; i < entities.size(); ++i)
            {
                PositionComponent& pc = m_world->getComponentManager().getComponent<PositionComponent>(entities[i]);
                pc.x = pc.y + pc.z;
                pc.y += 0.2f;
                pc.z = 1.f + pc.z * 3.f;


                std::cout << "Entity " << entities[i] << " | X: " << pc.x << " Y: " << pc.y << " Z: " << pc.z << std::endl;

            }
        }

        virtual bool shouldProcess()
        {
            return true;
        }
    };
}