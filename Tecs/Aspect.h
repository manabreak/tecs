#pragma once

#include "Utils/IndexedBitset.h"
#include "Types/Type.h"
#include "Component.h"
#include <memory>

namespace Tecs
{
    /* Aspect is a collection of "rules" about components. These rules are used 
       by systems to determine whether or not they are interested about entities. */
    class Aspect
    {
    public:
        const IndexedBitset& getAllSet();
        const IndexedBitset& getExclusionSet();
        const IndexedBitset& getOneSet();

        template<class T>
        Aspect* all()
        {
            m_allSet.set(Type<Component>::getIndexFor<T>(), true);
            return this;
        }

        template<class T1, class T2, class... Ts>
        Aspect* all()
        {
            all<T1>();
            all<T2, Ts...>();
            return this;
        }

        template<class T>
        Aspect* exclude()
        {
            m_exclusionSet.set(Type<Component>::getIndexFor<T>(), true);
            return this;
        }

        template<class T1, class T2, class... Ts>
        Aspect* exclude()
        {
            exclude<T1>();
            exclude<T2, Ts...>();
            return this;
        }

        template<class T>
        Aspect* one()
        {
            m_oneSet.set(Type<Component>::getIndexFor<T>(), true);
            return this;
        }

        template<class T1, class T2, class... Ts>
        Aspect* one()
        {
            one<T1>();
            one<T2, Ts...>();
            return this;
        }

        template<class T>
        static std::unique_ptr<Aspect> createAspectForAll()
        {
            Aspect* a = new Aspect();
            a->all<T>();
            return std::unique_ptr<Aspect>(a);
        }

        template<class T1, class T2, class... Ts>
        static std::unique_ptr<Aspect> createAspectForAll()
        {
            Aspect* a = new Aspect();
            a->all<T1, T2, Ts...>();
            return std::unique_ptr<Aspect>(a);
        }

        template<class T>
        static std::unique_ptr<Aspect> createAspectForOne()
        {
            Aspect* a = new Aspect();
            a->one<T>();
            return std::unique_ptr<Aspect>(a);
        }

        template<class T1, class T2, class... Ts>
        static std::unique_ptr<Aspect> createAspectForOne()
        {
            Aspect* a = new Aspect();
            a->one<T1, T2, Ts...>();
            return std::unique_ptr<Aspect>(a);
        }

        static std::unique_ptr<Aspect> createEmpty()
        {
            return std::unique_ptr<Aspect>(new Aspect());
        }

        Aspect(const Aspect&) = delete;
    private:
        Aspect();

        IndexedBitset m_allSet;
        IndexedBitset m_exclusionSet;
        IndexedBitset m_oneSet;
    };
}