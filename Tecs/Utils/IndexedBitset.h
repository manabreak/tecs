#pragma once

#include <unordered_map>
#include <vector>

// A simple wrapper for a vector of booleans.
// TODO: Bitwise operators could minimize the space - Try out the speed?
class IndexedBitset
{
public:
    IndexedBitset()
    {
    }

    void set(unsigned int index, bool value)
    {
        while (index >= m_bitset.size()) m_bitset.push_back(false);
        m_bitset[index] = value;
    }

    bool get(unsigned int index) const
    {
        if(index < m_bitset.size()) return m_bitset[index];
        return false;
    }

    bool isEmpty() const
    {
        return m_bitset.empty();
    }

    unsigned int size() const
    {
        return m_bitset.size();
    }

    void clear()
    {
        m_bitset.clear();
    }
private:
    std::vector<bool> m_bitset;
};
