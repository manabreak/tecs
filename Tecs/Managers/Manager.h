#pragma once

#include "../EntityObserver.h"

namespace Tecs
{
    class World;

    class Manager : public EntityObserver
    {
    public:
        virtual void entityAdded(unsigned int entity) {}
        virtual void entityChanged(unsigned int entity) {}
        virtual void entityDeleted(unsigned int entity) {}
        virtual void entityEnabled(unsigned int entity) {}
        virtual void entityDisabled(unsigned int entity) {}

        void setWorld(World& world)
        {
            m_world = &world;
        }

        World& getWorld()
        {
            return *m_world;
        }

    protected:
        World* m_world;
    };
}