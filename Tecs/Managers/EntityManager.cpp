#include "EntityManager.h"
#include "../Entity.h"
#include <iostream>
#include <algorithm>
#include "../World.h"

namespace Tecs
{

    EntityManager::EntityManager()
        : m_active(0),
        m_added(0),
        m_created(0),
        m_deleted(0)
    {

    }

    EntityManager::~EntityManager()
    {

    }

    unsigned int EntityManager::createEntityInstance()
    {
        unsigned int newID = m_identifierPool.checkOut();
        m_entities.push_back(newID);
        return newID;
    }

    void EntityManager::entityAdded(unsigned int id)
    {
        
    }

    void EntityManager::entityEnabled(unsigned int id)
    {
        m_disabled.erase(std::remove(m_disabled.begin(), m_disabled.end(), id), m_disabled.end());
    }

    void EntityManager::entityDisabled(unsigned int id)
    {
        m_disabled.push_back(id);
    }

    void EntityManager::entityDeleted(unsigned int id)
    {
        m_deleteQueue.push_back(id);
        if (!m_world->isProcessing())
        {
            clean();
        }
    }

    bool EntityManager::isEnabled(unsigned int id)
    {
        for (unsigned int i = 0; i < m_disabled.size(); ++i)
        {
            if (m_disabled[i] == id) return false;
        }
        return true;
    }

    unsigned int EntityManager::getEntityCount()
    {
        return m_entities.size();
    }

    unsigned long EntityManager::getTotalAdded()
    {
        return m_added;
    }

    unsigned long EntityManager::getTotalCreated()
    {
        return m_created;
    }

    unsigned long EntityManager::getTotalDeleted()
    {
        return m_deleted;
    }

    void EntityManager::clean()
    {
        unsigned int queueSize = m_deleteQueue.size();
        if (queueSize > 0)
        {
            for (unsigned int i = 0; i < queueSize; ++i)
            {
                m_disabled.erase(std::remove(m_disabled.begin(), m_disabled.end(), m_deleteQueue[i]), m_disabled.end());
                m_identifierPool.checkIn(m_deleteQueue[i]);
                m_active--;
                m_deleted++;
                m_entities[m_deleteQueue[i] - 1] = 0;
            }
            m_deleteQueue.clear();
        }
    }
}