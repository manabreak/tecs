#pragma once

#include <unordered_map>
#include <iostream>
#include <typeindex>
#include <functional>
#include <vector>

namespace Tecs
{
    template<class T>
    class Type
    {
    public:
        unsigned int getIndex() const
        {
            return m_index;
        }

        const std::type_index& getTypeIndex() const
        {
            return m_type;
        }

        static Type<T>& getTypeFor(std::type_index t)
        {
            for (unsigned int i = 0; i < S_INDICES.size(); ++i)
            {
                if (S_INDICES[i] == t) return S_TYPES[i];
            }

            S_INDICES.push_back(t);
            S_TYPES.push_back(Type<T>(t));
            return S_TYPES.back();
        }

        template<typename E>
        static Type<T>& getTypeFor()
        {
            return getTypeFor(std::type_index(typeid(E)));
        }

        template<typename E>
        static unsigned int getIndexFor()
        {
            return Type::getTypeFor(std::type_index(typeid(E))).m_index;
        }

        bool operator==(const Type<T>& other) const
        {
            return m_index == m_index && m_type == m_type;
        }
    private:
        static unsigned int S_INDEX;
        
        static std::vector<Type<T>> S_TYPES;
        static std::vector<std::type_index> S_INDICES;

        unsigned int m_index;
        const std::type_index m_type;

        Type(std::type_index& type)
            : m_index(S_INDEX++),
            m_type(type)
        {
        }
    };

    template<class T>
    unsigned int Type<T>::S_INDEX = 0;

    template<class T>
    std::vector<Type<T>> Type<T>::S_TYPES;

    template<class T>
    std::vector<std::type_index> Type<T>::S_INDICES;
}

namespace std
{
    template <class T>
    struct hash<Tecs::Type<T>>
    {
        std::size_t operator()(const Tecs::Type<T>& type) const
        {
            return type.getTypeIndex().hash_code();
        }
    };
}
