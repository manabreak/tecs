#pragma once

#include "Managers/ComponentManager.h"
#include "Managers/EntityManager.h"
#include "Component.h"
#include "Types/Type.h"
#include "Utils/IndexedBitset.h"
#include <string>
#include <memory>
#include <functional>

namespace Tecs
{
    class World;

    /* Entity class. Base class for all entities. */
    class Entity
    {
    public:
        // Constructor
        Entity(World& world, int id);

        ~Entity();

        bool operator==(const Entity& other) const;

        // Retrieves the ID of this entity
        unsigned int getId() const;

        // Retrieves the universal unique ID of this entity
        unsigned int getUUID() const;

        // Attaches a new component to this entity
        template<typename E>
        void addComponent(E* component)
        {
            m_world.getComponentManager().addComponent(m_id, Type<Component>::getTypeFor<E>(), component);
        }

        template<typename E>
        E& addComponent()
        {
            return m_world.getComponentManager().addComponent<E>(m_id);
        }

        template<typename E, typename... Params>
        E& addComponent(Params&&... params)
        {
            return m_world.getComponentManager().addComponent<E>(m_id, params...);
        }
        
        // Detaches a component from this entity
        template<typename E>
        void removeComponent()
        {
            m_world.getComponentManager().removeComponent(m_id, Type<Component>::getTypeFor<E>());
        }

        // Retrieves a component attached to this entity
        template<typename E>
        E& getComponent() const
        {
            return (E&) m_world.getComponentManager().getComponent<E>(m_id);
        }

        // Returns true if the given component type is attached to this entity
        template<typename E>
        bool hasComponent() const
        {
            return m_world.getComponentManager().hasComponent<E>(m_id);
        }

        // Retrieves all the components attached to this entity
        const std::vector<Component*>& getComponents();

        // Adds this entity to a group
        void addToGroup(const std::string& group);

        // Retrieves the group of this entity
        std::vector<std::string>& getGroups();

        // Sets a tag for this entity
        void setTag(const std::string& tag);

        // Retrieves the tag set for this entity
        const std::string& getTag();

        // Removes this entity from the world
        void remove();

        // Checks whether or not this entity is enabled
        bool isEnabled();

        void changedInWorld();

        // Refreshes the state of this entity
        void refresh();

        // Enables this entity
        void enable();

        // Disables this entity
        void disable();
    protected:
        // Reset
        void reset();
    private:
        // Static universal unique ID, runs from 0 onwards
        static unsigned int S_UUID;
        // The universal unique ID of this entity
        unsigned int m_uuid;
        // The index of this entity
        unsigned int m_id;
        // The world  this entity is in
        World& m_world;
    };
}
