#pragma once

#include <iostream>

#include "Managers/EntityManager.h"
#include "Managers/ComponentManager.h"
#include "Managers/TagManager.h"
#include "Managers/GroupManager.h"
#include "System.h"
#include <unordered_map>
#include "Types/Type.h"
#include <type_traits>
#include "Utils/Pooler.hpp"
#include "ComponentMapper.h"
#include "Entity.h"

namespace Tecs
{
    class World
    {
    public:
        World();
        World(const World&) = delete;
        ~World();

        EntityManager& getEntityManager();
        ComponentManager& getComponentManager();
        TagManager& getTagManager();
        GroupManager& getGroupManager();

        template<class T>
        T& addManager()
        {
            unsigned int index = Type<Manager>::getIndexFor<T>();
            if (index < m_managers.size() && m_managers[index] != nullptr)
            {
                return (T&) *m_managers[index];
            }

            if (index >= m_managers.size())
            {
                m_managers.resize(index * 3 / 2 + 1);
            }

            T* t = Pooler<T>::instance().create();
            m_managers[index] = t;
            m_managers[index]->setWorld(*this);
            return (T&) *t;
        }

        template<class T, class... Params>
        T& addManager(Params&&... params)
        {
            unsigned int index = Type<Manager>::getIndexFor<T>();
            if (index < m_managers.size() && m_managers[index] != nullptr)
            {
                return (T&) *m_managers[index];
            }

            if (index >= m_managers.size())
            {
                m_managers.resize(index * 3 / 2 + 1);
            }

            T* t = Pooler<T>::instance().getNext();
            t = new(t) T(std::forward<Params>(params)...);
            m_managers[index] = t;
            m_managers[index]->setWorld(*this);
            return (T&) *t;
        }

        template<class T>
        T& getManager()
        {
            return (T&) *m_managers[Type<Manager>::getIndexFor<T>()];
        }

        template<class T>
        void deleteManager()
        {
            unsigned int index = Type<Manager>::getIndexFor<T>();
            if (index < m_managers.size())
            {
                T* t = (T*) m_managers[index];
                Pooler<T>::instance().free(t);
                m_managers[index] = nullptr;
            }
        }

        template<class T>
        T& addSystem()
        {
            unsigned int index = Type<System>::getIndexFor<T>();

            if (index < m_systems.size() && m_systems[index] != nullptr) 
            {
                return (T&) m_systems[index];
            }

            if (index >= m_systems.size())
            {
                m_systems.resize(index * 3 / 2 + 1);
            }

            T* t = (T*) Pooler<T>::instance().create();
            m_systems[index] = t;
            m_systems[index]->setWorld(this);
            m_systems[index]->setSystemIndexInternal(index);
            return (T&) *t;
        }

        template<class T, class... Params>
        T& addSystem(Params&&... params)
        {
            unsigned int index = Type<System>::getIndexFor<T>();

            if (index < m_systems.size() && m_systems[index] != nullptr)
            {
                return (T&) m_systems[index];
            }

            if (index >= m_systems.size())
            {
                m_systems.resize(index * 3 / 2 + 1);
            }
           
            T* t = Pooler<T>::instance().getNext();
            t = new (t) T(std::forward<Params>(params)...);

            m_systems[index] = t;
            m_systems[index]->setWorld(this);
            m_systems[index]->setSystemIndexInternal(index);
            return (T&) *t;
        }

        template<class T>
        T& getSystem()
        {
            return (T&) *m_systems[Type<System>::getIndexFor<T>()];
        }

        template<class T>
        void deleteSystem()
        {
            unsigned int index = Type<System>::getIndexFor<T>();
            if (index < m_systems.size())
            {
                T* t = (T*) m_systems[index];
                Pooler<T>::instance().free(t);
                m_systems[index] = nullptr;
            }
        }

        const std::vector<System*>& getSystems();

        // Deletes an entity and all the components attached to it
        void deleteEntity(unsigned int id);
        void changedEntity(unsigned int id);
        void enableEntity(unsigned int id);
        void disableEntity(unsigned int id);

        Entity createEntity();

        float getDelta();
        void setDelta(float delta);

        // Main processing method, handles all the changes in the world.
        // Updates only the active systems.
        void process();

        void process(float delta);

        // Processes a single system, whether or not it's marked as passive
        template<class T>
        void processSystem()
        {
            processSystem(Type<System>::getIndexFor<T>());
        }

        template<class T>
        void processSystem(float delta)
        {
            processSystem(Type<System>::getIndexFor<T>(), delta);
        }

        void processSystem(unsigned int index);
        void processSystem(unsigned int index, float delta);

        int getEntityCount();

        bool isProcessing();

        ComponentMapper& getComponentMapper();
    private:
        ComponentMapper m_componentMapper;
        std::vector<unsigned int> m_added;
        std::vector<unsigned int> m_changed;
        std::vector<unsigned int> m_deleted;
        std::vector<unsigned int> m_enable;
        std::vector<unsigned int> m_disable;

        float m_delta;

        std::vector<Manager*> m_managers;

        std::vector<System*> m_systems;

        bool m_processing;

        void processAdded();
        void processChanged();
        void processDeleted();
        void processEnabled();
        void processDisabled();
    };
}
