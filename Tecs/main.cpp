#include "World.h"
#include "Components/PositionComponent.h"
#include "Systems/PositionSystem.h"

using namespace Tecs;

int main()
{
    World world;

    world.addSystem<PositionSystem>();

    
    for (unsigned int i = 0; i < 4; ++i)
    {
        unsigned int entity = world.createEntity().getId();
        world.getComponentManager().addComponent<PositionComponent>(entity);
    }

    std::cout << "First process()" << std::endl;
    world.process();

    world.deleteEntity(3);

    world.process();

    world.disableEntity(1);

    world.process();

    world.enableEntity(1);

    world.process();

    return 0;
}