#pragma once
#include <functional>

namespace Tecs
{
    /* Base class for all the components. */
    class Component
    {
    public:
        Component() : m_enabled(true) {}
        Component(const Component&) = delete;

        void _setDtorInternal(std::function<void()>& dtorFnc)
        {
            m_dtorFnc = dtorFnc;
        }

        void _callDtorInternal()
        {
            m_dtorFnc();
        }

        void _setEntityIDInternal(unsigned int entityID)
        {
            m_entityID = entityID;
        }


        unsigned int getEntityID() const
        {
            return m_entityID;
        }

        bool isEnabled()
        {
            return m_enabled;
        }

        void enable()
        {
            m_enabled = true;
        }

        void disable()
        {
            m_enabled = false;
        }
    private:
        bool m_enabled;
        std::function<void()> m_dtorFnc;
        unsigned int m_entityID;
    };
}