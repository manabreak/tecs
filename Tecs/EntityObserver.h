#pragma once

namespace Tecs
{
    /* Interface for entity observers. Mainly used by managers and systems. */
    class EntityObserver
    {
    public:
        virtual void entityAdded(unsigned int id) = 0;
        virtual void entityChanged(unsigned int id) = 0;
        virtual void entityDeleted(unsigned int id) = 0;
        virtual void entityEnabled(unsigned int id) = 0;
        virtual void entityDisabled(unsigned int id) = 0;
    };
}