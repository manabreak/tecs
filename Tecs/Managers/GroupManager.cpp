#include "GroupManager.h"
#include "../Entity.h"
#include <cstring>

namespace Tecs
{
    GroupManager::GroupManager()
    {
    }

    void GroupManager::add(Entity& entity, const std::string& group)
    {
        add(entity.getId(), group);
    }

    void GroupManager::add(unsigned int id, const std::string& group)
    {
        if (m_entitiesByGroup.find(group) == m_entitiesByGroup.end())
        {
            m_entitiesByGroup[group] = std::vector<unsigned int>();
        }
        m_entitiesByGroup[group].push_back(id);

        if (id >= m_groupsByEntity.size())
        {
            m_groupsByEntity.resize(id * 3 / 2 + 1);
        }
        m_groupsByEntity[id].push_back(group);
    }

    void GroupManager::remove(Entity& entity, const std::string& group)
    {
        remove(entity.getId(), group);
    }

    void GroupManager::remove(unsigned int id, const std::string& group)
    {
        if (m_entitiesByGroup.find(group) != m_entitiesByGroup.end())
        {
            for (unsigned int i = 0; i < m_entitiesByGroup[group].size(); ++i)
            {
                if (m_entitiesByGroup[group][i] == id)
                {
                    m_entitiesByGroup[group].erase(std::remove(m_entitiesByGroup[group].begin(), m_entitiesByGroup[group].end(), id), m_entitiesByGroup[group].end());
                    break;
                }
            }
        }

        if (id < m_groupsByEntity.size())
        {
            for (unsigned int i = 0; i < m_groupsByEntity[id].size(); ++i)
            {
                if (std::strcmp(m_groupsByEntity[id][i].c_str(), group.c_str()))
                {
                    std::swap(m_groupsByEntity[id][i], m_groupsByEntity[id].back());
                    m_groupsByEntity[id].pop_back();
                    break;
                }
            }
        }
    }

    void GroupManager::removeFromAllGroups(Entity& entity)
    {
        removeFromAllGroups(entity.getId());
    }

    void GroupManager::removeFromAllGroups(unsigned int id)
    {
        if (id < m_groupsByEntity.size())
        {
            m_groupsByEntity[id].clear();
        }
    }

    std::vector<unsigned int>& GroupManager::getEntities(const std::string& group)
    {
        if (m_entitiesByGroup.find(group) == m_entitiesByGroup.end())
        {
            m_entitiesByGroup[group] = std::vector<unsigned int>();
        }
        return m_entitiesByGroup[group];
    }

    std::vector<std::string>& GroupManager::getGroups(Entity& entity)
    {
        return getGroups(entity.getId());
    }

    std::vector<std::string>& GroupManager::getGroups(unsigned int id)
    {
        if (id >= m_groupsByEntity.size())
        {
            m_groupsByEntity.resize(id * 3 / 2);
        }
        return m_groupsByEntity[id];
    }

    bool GroupManager::isInAnyGroup(Entity& entity)
    {
        return isInAnyGroup(entity.getId());
    }

    bool GroupManager::isInAnyGroup(unsigned int id)
    {
        return id < m_groupsByEntity.size() && m_groupsByEntity[id].size() > 0;
    }

    bool GroupManager::isInGroup(Entity& entity, const std::string& group)
    {
        return isInGroup(entity.getId(), group);
    }

    bool GroupManager::isInGroup(unsigned int id, const std::string& group)
    {
        if (m_entitiesByGroup.find(group) != m_entitiesByGroup.end())
        {
            if (id < m_groupsByEntity.size())
            {
                for (unsigned int i = 0; i < m_groupsByEntity[id].size(); ++i)
                {
                    if (std::strcmp(group.c_str(), m_groupsByEntity[id][i].c_str()))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    void GroupManager::entityDeleted(Entity& entity)
    {
        removeFromAllGroups(entity);
    }
}
