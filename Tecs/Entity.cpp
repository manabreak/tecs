#include "Entity.h"
#include "Types/Type.h"
#include "World.h"

namespace Tecs
{

    unsigned int Entity::S_UUID = 0;

    Entity::Entity(World& world, int id)
        : m_world(world),
        m_id(id),
        m_uuid(++S_UUID)
    {
    }

    Entity::~Entity()
    {
    }

    bool Entity::operator==(const Entity& other) const
    {
        return m_id == other.m_id && m_uuid == other.m_uuid;
    }

    void Entity::reset()
    {
        m_uuid = ++S_UUID;
    }

    void Entity::addToGroup(const std::string& group)
    {
        m_world.getGroupManager().add(m_id, group);
    }

    std::vector<std::string>& Entity::getGroups()
    {
        return m_world.getGroupManager().getGroups(m_id);
    }

    void Entity::setTag(const std::string& tag)
    {
        m_world.getTagManager().setTag(tag, m_id);
    }

    const std::string& Entity::getTag()
    {
        return m_world.getTagManager().getTag(m_id);
    }

    void Entity::remove()
    {
        m_world.deleteEntity(m_id);
    }

    bool Entity::isEnabled()
    {
        return m_world.getEntityManager().isEnabled(m_id);
    }

    const std::vector<Component*>& Entity::getComponents()
    {
        return m_world.getComponentManager().getComponentsFor(m_id);
    }

    void Entity::changedInWorld()
    {
        m_world.changedEntity(m_id);
    }

    void Entity::refresh()
    {
        /// TODO: Use of function?
    }

    void Entity::enable()
    {
        m_world.enableEntity(m_id);
    }

    void Entity::disable()
    {
        m_world.disableEntity(m_id);
    }

    unsigned int Entity::getId() const
    {
        return m_id;
    }

    unsigned int Entity::getUUID() const
    {
        return m_uuid;
    }

}
