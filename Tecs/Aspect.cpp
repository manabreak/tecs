#include "Aspect.h"

namespace Tecs
{
    Aspect::Aspect()
    {

    }

    const IndexedBitset& Aspect::getAllSet()
    {
        return m_allSet;
    }

    const IndexedBitset& Aspect::getExclusionSet()
    {
        return m_exclusionSet;
    }

    const IndexedBitset& Aspect::getOneSet()
    {
        return m_oneSet;
    }
}