#pragma once

#include "Manager.h"
#include "../Types/Type.h"
#include "../Component.h"
#include "../Utils/Pooler.hpp"

#include <memory>
#include <vector>
#include <functional>

namespace Tecs
{
    class Component;

    class ComponentManager : public Manager
    {
    public:
        ComponentManager();
        ~ComponentManager();

        template<class T>
        T& addComponent(unsigned int entity)
        {
            unsigned int index = Type<Component>::getIndexFor<T>();

            if (index < m_componentsByType.size())
            {
                auto& components = m_componentsByType[index];
                if (entity < components.size() && components[entity] != nullptr)
                {
                    return (T&) components[entity];
                }
            }

            if (index >= m_componentsByType.size())
            {
                m_componentsByType.resize(index * 3 / 2 + 1);
            }

            auto& components = m_componentsByType[index];
            if (entity >= components.size())
            {
                components.resize(entity * 3 / 2 + 1);
            }

            T* t = Pooler<T>::instance().create();
            t->_setEntityIDInternal(entity);
            void* c = (void*) t;
            std::function<void()> dtor = [c]()
            {
                Pooler<T>::instance().free((T*)c);
            };
            t->_setDtorInternal(dtor);
            components[entity] = t;
            flagEntityComponentBit(entity, index, true);
            return (T&) *t;
        }

        template<class T, class... Params>
        T& addComponent(unsigned int entity, Params&&... params)
        {
            unsigned int index = Type<Component>::getIndexFor<T>();

            if (index < m_componentsByType.size())
            {
                auto& components = m_componentsByType[index];
                if (entity < components.size() && components[entity] != nullptr)
                {
                    return (T&) components[entity];
                }
            }

            if (index >= m_componentsByType.size())
            {
                m_componentsByType.resize(index * 3 / 2 + 1);
            }

            if (entity >= m_componentsByType[index].size())
            {
                m_componentsByType[index].resize(entity * 3 / 2 + 1);
            }

            T* t = Pooler<T>::instance().getNext();
            t = new (t) T(std::forward<Params>(params)...);
            t->_setEntityIDInternal(entity);
            void* c = (void*) t;
            std::function<void()> dtor = [c]()
            {
                Pooler<T>::instance().free((T*) c);
            };
            t->_setDtorInternal(dtor);
            m_componentsByType[index][entity] = t;
            flagEntityComponentBit(entity, index, true);
            return (T&) *t;
        }

        void removeComponent(unsigned int entity, const Type<Component>& type);
        virtual void initialize() {}

        template<class T>
        std::vector<T>& getComponentsByType()
        {
            return (std::vector<T>&) m_componentsByType[(Type<Component>::getIndexFor<T>())];
        }
        
        template<class T>
        T& getComponent(unsigned int entity) const
        {
            return (T&) *getComponentOfEntity(Type<Component>::getIndexFor<T>(), entity);
        }

        template<class T>
        bool hasComponent(unsigned int entity) const
        {
            unsigned int index = Type<Component>::getIndexFor<T>();
            if (m_componentsByType.size() > index &&
                m_componentsByType[index].size() > entity &&
                m_componentsByType[index][entity] != nullptr) return true;
            return false;
        }

        const std::vector<Component*>& getComponentsFor(unsigned int entity);

        virtual void entityDeleted(unsigned int entity);

        void clean();

        unsigned long int getComponentCount()
        {
            long int count = 0;
            for (unsigned int i = 0; i < m_componentsByType.size(); ++i)
            {
                count += m_componentsByType[i].size();
            }
            return count;
        }
    private:
        void ensureCapacity(unsigned int typeIndex, unsigned int entityId);
        std::vector<std::vector<Component*>> m_componentsByType;
        std::vector<std::vector<Component*>> m_componentsByEntity;
        std::vector<unsigned int> m_deleted;

        void flagEntityComponentBit(unsigned int entity, unsigned int index, bool status);
        Component* getComponentOfEntity(unsigned int idxOfComponent, unsigned int entity) const;
        void removeComponentsOfEntity(unsigned int entity);
    };
}
