#include "TagManager.h"
#include "../Entity.h"
#include <cstring>

namespace Tecs
{
    namespace {
        const std::string UNSET_TAG = "";
    }

    TagManager::TagManager()
    {
    }

    void TagManager::setTag(const std::string& tag, unsigned int entity)
    {
        if (entity >= m_tagsByEntity.size())
        {
            m_tagsByEntity.resize(entity * 3 / 2 + 1);
        }
        m_tagsByEntity[entity] = tag;
    }

    const std::string& TagManager::getTag(unsigned int entity)
    {
        if (entity < m_tagsByEntity.size())
        {
            return m_tagsByEntity[entity];
        }
        return Tecs::UNSET_TAG;
    }

    void TagManager::unsetTag(const std::string& tag)
    {
        for (unsigned int i = 0; i < m_tagsByEntity.size(); ++i)
        {
            if (std::strcmp(m_tagsByEntity[i].c_str(), tag.c_str()))
            {
                m_tagsByEntity[i] = "";
            }
        }
    }

    bool TagManager::isTagSet(const std::string& tag)
    {
        for (unsigned int i = 0; i < m_tagsByEntity.size(); ++i)
        {
            if (std::strcmp(m_tagsByEntity[i].c_str(), tag.c_str())) return true;
        }
        return false;
    }

    const std::vector<std::string>& TagManager::getAllTags()
    {
        return m_tagsByEntity;
    }

    void TagManager::entityDeleted(unsigned int entity)
    {
        if (entity < m_tagsByEntity.size())
        {
            std::string tag = m_tagsByEntity[entity];
            m_tagsByEntity[entity] = "";
        }
    }
}
