#pragma once

#include "Manager.h"
#include <vector>
#include <unordered_map>
#include <string>

namespace Tecs
{
    class Entity;

    class GroupManager : public Manager
    {
    public:
        GroupManager();
        void add(Entity& entity, const std::string& group);
        void add(unsigned int entity, const std::string& group);
        void remove(Entity& entity, const std::string& group);
        void remove(unsigned int entity, const std::string& group);
        void removeFromAllGroups(Entity& entity);
        void removeFromAllGroups(unsigned int entity);

        // std::vector<Entity*>& getEntities(const std::string& group);
        std::vector<unsigned int>& getEntities(const std::string& group);
        std::vector<std::string>& getGroups(Entity& entity);
        std::vector<std::string>& getGroups(unsigned int entity);


        bool isInAnyGroup(Entity& entity);
        bool isInAnyGroup(unsigned int entity);
        bool isInGroup(Entity& entity, const std::string& group);
        bool isInGroup(unsigned int entity, const std::string& group);
        virtual void entityDeleted(Entity& entity);
        virtual void initialize() {}
    private:
        std::unordered_map<std::string, std::vector<unsigned int>> m_entitiesByGroup;
        std::vector<std::vector<std::string>> m_groupsByEntity;
    };
}