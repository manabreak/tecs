#pragma once

#include <vector>
#include "Utils/IndexedBitset.h"

namespace Tecs
{
    class World;
    class ComponentManager;

    // Responsible for wiring components to the entities
    class ComponentMapper
    {
    public:
        ComponentMapper(World& world);
        ~ComponentMapper();

        void entityCreated(unsigned int entity);
        void entityRemoved(unsigned int entity);
        void entityDisabled(unsigned int entity);
        void entityEnabled(unsigned int entity);
        void entityChanged(unsigned int entity);

        void attachComponent(unsigned int entity, unsigned int componentType);
        void detachComponent(unsigned int entity, unsigned int componentType);
        bool isComponentAttached(unsigned int entity, unsigned int component);
        IndexedBitset& getComponentBitset(unsigned int entity);

        void setSystemBit(unsigned int entity, unsigned int systemIndex, bool value);
        bool getSystemBit(unsigned int entity, unsigned int systemIndex);
        IndexedBitset& getSystemBitset(unsigned int entity);
    private:
        World& m_world;
        std::vector<IndexedBitset> m_systemBits;
        std::vector<IndexedBitset> m_componentBits;
    };
}